.label-title{
    -fx-font-size: 20pt;
    -fx-font-weight:bold;
    -fx-text-fill:chocolate;
    
}
.label-name{
    -fx-font-size:15pt;
    -fx-text-fill:gold;
    -fx-font-weight:300;
}
.label-result{
    -fx-font-size:18pt;
    -fx-text-fill:red;
    -fx-font-weight:700;
}
.button-mix{
    -fx-background-color: linear-gradient(aquamarine,blue);
    -fx-text-fill:black;
    -fx-font-size: 14pt;
    -fx-background-radius:4;
}
.button-red{
    -fx-background-color:linear-gradient(red,yellow);
    -fx-text-fill:black;
    -fx-font-size: 14pt;
    -fx-background-radius:4;
}
.button-gold{
    -fx-background-color: linear-gradient(gold,goldenrod);
    -fx-text-fill:black;
    -fx-font-size: 14pt;
    -fx-background-radius:4;
}
.button-colorMix{
    -fx-background-color: linear-gradient(coral,chocolate);
    -fx-text-fill:black;
    -fx-font-size: 14pt;
    -fx-background-radius:4;
}
.textField-all{
    -fx-font-size: 12pt;
    -fx-text-fill: blueviolet;
}




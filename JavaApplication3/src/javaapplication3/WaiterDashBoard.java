package javaapplication3;
public class WaiterDashBoard {
    private String customerName;
    private int tableNumber;

    public String getCustomerName() {
        return customerName;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public WaiterDashBoard() {
    }

    public WaiterDashBoard(String customerName, int tableNumber) {
        this.customerName = customerName;
        this.tableNumber = tableNumber;
    }
    
}

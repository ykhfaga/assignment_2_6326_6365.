package javaapplication3;
public class Customer extends User {
    
    private int tablereservednumber;

    public int getTablereservednumber() {
        return tablereservednumber;
    }

    public void setTablereservednumber(int tablereservednumber) {
        this.tablereservednumber = tablereservednumber;
    }

    public Customer(String name,String role, int tablereservednumber ) {
        super(name, role);
        this.tablereservednumber = tablereservednumber;
    }
    public Customer(String name,String role)
    {
        super(name,role);
    }

    public Customer() {
    }
    
}
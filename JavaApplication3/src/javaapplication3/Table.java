package javaapplication3;
public class Table extends Restaurant {
    private int tablenumber;
    private int tableseats;
    private boolean smokingornot;
    private boolean reservedornot;

    public Table(int tablenumber, int tableseats, boolean smokingornot, boolean reservedornot) {
        this.tablenumber = tablenumber;
        this.tableseats = tableseats;
        this.smokingornot = smokingornot;
        this.reservedornot = reservedornot;
    }
    
    
    public Table(int tableNumber, int numberOfSeats, boolean smoking) {
        this.tablenumber = tableNumber;
        this.tableseats = numberOfSeats;
        this.smokingornot = smoking;
    }

    public Table() {
    }

    public int getTableseats() {
        return tableseats;
    }

    public void setTableseats(int tableseats) {
        this.tableseats = tableseats;
    }

    public int getTablenumber() {
        return tablenumber;
    }

    public void setTablenumber(int tablenumber) {
        this.tablenumber = tablenumber;
    }

    public boolean getReservedornot() {
        return reservedornot;
    }

    public boolean getSmokingornot() {
        return smokingornot;
    }

    public void setReservedornot(Boolean reservedornot) {
        this.reservedornot = reservedornot;
    }

    public void setSmokingornot(Boolean smokingornot) {
        this.smokingornot = smokingornot;
    }

   
}

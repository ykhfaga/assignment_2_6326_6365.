/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication3;

/**
 *
 * @author abdallah
 */
public class Appetizer extends Dish implements DishInt{

    private final double taxes=0.1;
    
    public Appetizer(String name,String type, float price,int quantity) {
        super(name,price,type,quantity);
    }
    
    public Appetizer() {
    }
    public Appetizer(String name,String type, float price) {
        super(name,price,type);
    }
    
    @Override
    public float calcTotalPrice(double price,int quantity ) {
        Dish.totalPrice=(float) (Dish.totalPrice+price*quantity+(price*quantity*taxes));
        return Dish.totalPrice;
        //To change body of generated methods, choose Tools | Templates.
    }
    
}

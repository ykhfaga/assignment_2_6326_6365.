package javaapplication3;

public abstract class Dish implements DishInt{
    public static float totalPrice=0;
    private String dishname;
    private float dishprice;
    private String dishsort;
    private int numberofdishes;


    public Dish(){}
    public Dish(String name,float price,String sort ,int numberofdishes)
    {
    this.dishname=name;
    this.dishprice=price;
    this.dishsort=sort;
    this.numberofdishes=numberofdishes;
    }
     public Dish(String dishName, float price, String type) {
        this.dishname = dishName;
        this.dishprice = price;
        this.dishsort = type;
    }
    public String getDishsort() {
        return dishsort;
    }

    public int getNumberofdishes() {
        return numberofdishes;
    }

    public void setNumberofdishes(int numberofdishes) {
        this.numberofdishes = numberofdishes;
    }
    
    
    
    public void setDishsort(String dishsort) {
        this.dishsort = dishsort;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public float getDishprice() {
        return dishprice;
    }

    public void setDishprice(float dishprice) {
        this.dishprice = dishprice;
    }

    
    
}

package javaapplication3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
public class Restaurant{
    
    private static List<User> users =  new ArrayList<>();
    private static List<Dish> dishes =  new ArrayList<>();
    private static List<Table> tables =  new ArrayList<>();
    private static List<Customer> customerSaved = new ArrayList<>();
    private static List<Dish> dishesServed = new ArrayList<>();
    
    public static void load() throws JDOMException, IOException {
         
         Restaurant.users.clear();
         Restaurant.dishes.clear();
         
         
         File inputFile = new File("input.xml");
         SAXBuilder saxBuilder = new SAXBuilder();
         Document document = saxBuilder.build(inputFile);
         
         Element rootNode = document.getRootElement();
         //System.out.println(document.getRootElement().getName());
         
         Element usersList =  rootNode.getChild("users");
         List<Element> userList =  usersList.getChildren("user");
         
         
         
         Element dishesList = rootNode.getChild("dishes");
         List<Element> dishList = dishesList.getChildren("dish");
        
         for (int temp = 0; temp < userList.size(); temp++) {    
            Element user = userList.get(temp);
            
            String name;
            String role;
            String username;
            String password;
            
            name=user.getChild("name").getText();
            role=user.getChild("role").getText();
            username=user.getChild("username").getText();
            password=user.getChild("password").getText();
            
            Restaurant.users.add(new User(name,role,username,password));
         }
     
        for (int temp = 0; temp < dishList.size(); temp++){
            Element dish = dishList.get(temp);
            
            String dishName = dish.getChild("name").getText();
            float dishPrice = Float.parseFloat(dish.getChild("price").getText());
            String dishType = dish.getChild("type").getText();
            
            if(dishName.equals("Appetizer")){
                Dish a = new Appetizer(dishName,dishType,dishPrice);
                Restaurant.dishes.add(a);
            }else if(dishName.equals("Main Course")){
                Dish a = new MainCourse(dishName,dishType,dishPrice);
                Restaurant.dishes.add(a);
            }else{
                Dish a = new Dessert(dishName,dishType,dishPrice);
                Restaurant.dishes.add(a);
            }
            
            
        }
       
    }
    public static void loadTables() throws JDOMException, IOException {
         
         Restaurant.tables.clear();
         
         File inputFile = new File("input.xml");
         SAXBuilder saxBuilder = new SAXBuilder();
         Document document = saxBuilder.build(inputFile);
         
         Element rootNode = document.getRootElement();
         
         Element tablesList = rootNode.getChild("tables");
         List<Element> tableList = tablesList.getChildren("table");
        
         for (int temp = 0; temp < tableList.size(); temp++){
             Element table = tableList.get(temp);
             
             int tableNumber=Integer.parseInt(table.getChild("number").getText());
             int numberOfSeats=Integer.parseInt(table.getChild("number_of_seats").getText());
             boolean smoking=Boolean.parseBoolean(table.getChild("smoking").getText());
             
             Restaurant.tables.add(new Table(tableNumber,numberOfSeats,smoking,false));
         }
         
       
    }
     public static void write(User a)throws JDOMException,IOException{
        
        File input = new File("input.xml");
        SAXBuilder sb = new SAXBuilder();
        Document document = sb.build(input);
        Element rootNode = document.getRootElement();
        
            
        
        String nameElement = "name";
        String roleElement = "role";
        String userNameElement = "username";
        String passwordElement = "password";
        
        Element usersList =  rootNode.getChild("users");
        List<Element> userList =  usersList.getChildren("user");
        Element newUser = new Element("user");
        newUser.addContent(new Element(nameElement).setText(a.getName()));
        newUser.addContent(new Element(roleElement).setText(a.getRole()));
        newUser.addContent(new Element(userNameElement).setText(a.getUsername()));
        newUser.addContent(new Element(passwordElement).setText(a.getPassword()));
        userList.add(newUser);
        try {
        FileWriter writer = new FileWriter("input.xml");
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        outputter.output(document, writer);
        //outputter.output(document, System.out);
        writer.close();
    } catch (IOException e) {
    }
    }
    public static void writeRecent(String customerName,int tableNumber,float totalPrice,List<Dish> dish5) throws JDOMException, IOException {

        
        File input = new File("customer.xml");
        SAXBuilder sb = new SAXBuilder();
        Document document = sb.build(input);
        Element rootNode = document.getRootElement();
 

        String nameElement = "name";
        String totalPriceElement = "totalprice";
        String tableNumberElement = "tableNumber";
        String checkOutElement = "checkout";
        String dishNameElement = "dishName";
        String dishTypeElement = "dishType";
        String dishPriceElement = "dishPrice";
        String quantityElement = "quantity";
        String servedElement ="served";
        String servicedElement="serviced";
        List<Element> customerList = rootNode.getChildren("customer");
        Element newCustomer = new Element("customer");
        newCustomer.addContent(new Element(nameElement).setText(customerName));
        newCustomer.addContent(new Element(totalPriceElement).setText(String.valueOf(totalPrice)));
        newCustomer.addContent(new Element(tableNumberElement).setText(String.valueOf(tableNumber)));
        newCustomer.addContent(new Element(checkOutElement).setText("false"));
        Element newDishes = new Element("dishes");
        for(int i=0;i<dish5.size();i++){
            Element dish2 = new Element("dish");
            dish2.addContent(new Element(dishNameElement).setText(dish5.get(i).getDishname()));
            dish2.addContent(new Element(dishTypeElement).setText(dish5.get(i).getDishsort()));
            dish2.addContent(new Element(dishPriceElement).setText(String.valueOf(dish5.get(i).getDishprice())));
            dish2.addContent(new Element(quantityElement).setText(String.valueOf(dish5.get(i).getNumberofdishes())));
            newDishes.addContent(dish2);
             
        }
        newCustomer.addContent(newDishes);
        newCustomer.addContent(new Element(servedElement).setText("false"));
        newCustomer.addContent(new Element(servicedElement).setText("false"));
        customerList.add(newCustomer);
        try {
            FileWriter writer = new FileWriter("customer.xml");
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            outputter.output(document, writer);
            //outputter.output(document, System.out);
            writer.close();
        } catch (IOException e) {
        }
    }
    public static void modifyServed(int tableNumber) throws JDOMException,IOException{
        File input = new File("customer.xml");
        SAXBuilder sb = new SAXBuilder();
        Document document = sb.build(input);
        Element rootNode = document.getRootElement();
        List<Element> customersList = rootNode.getChildren("customer");
        for (int i = 0; i < customersList.size(); i++) {
            Element customerX = customersList.get(i);
            int tableNumber1 = Integer.parseInt(customerX.getChild("tableNumber").getText());
            boolean checkOut5 = Boolean.parseBoolean(customerX.getChild("checkout").getText());
            if(tableNumber1==tableNumber && checkOut5==false){
                customerX.getChild("served").setText("true");
            }
        }
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(document,new FileOutputStream("customer.xml"));
            
    }
    public static void modifyServiced(int tableNumber1) throws JDOMException,IOException{
        File input = new File("customer.xml");
        SAXBuilder sb = new SAXBuilder();
        Document document = sb.build(input);
        Element rootNode = document.getRootElement();
        List<Element> customersList = rootNode.getChildren("customer");
        for (int i = 0; i < customersList.size(); i++) {
            Element customerX = customersList.get(i);
            int tableNumber = Integer.parseInt(customerX.getChild("tableNumber").getText());
            boolean checkOut5 = Boolean.parseBoolean(customerX.getChild("checkout").getText());
            if(tableNumber1==tableNumber && checkOut5==false){
                customerX.getChild("serviced").setText("true");
            }
        }
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(document,new FileOutputStream("customer.xml"));
            
    }

    public static void modifyCheckOut(String name,int tableNumber) throws JDOMException,IOException{
        File input = new File("customer.xml");
        SAXBuilder sb = new SAXBuilder();
        Document document = sb.build(input);
        Element rootNode = document.getRootElement();
        List<Element> customersList = rootNode.getChildren("customer");
        for (int i = 0; i < customersList.size(); i++) {
            Element customerX = customersList.get(i);
            String name1 = customerX.getChild("name").getText();
            int tableNumber1 = Integer.parseInt(customerX.getChild("tableNumber").getText());
            if(tableNumber1==tableNumber && name1.equals(name)){
                customerX.getChild("checkout").setText("true");
                customerX.getChild("serviced").setText("true");
                customerX.getChild("served").setText("true");
            }
        }
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(document,new FileOutputStream("customer.xml"));
            
    }
    public static List<User> getUsers() {
        return users;
    }

    public static void setDishes(List<Dish> dishes) {
        Restaurant.dishes = dishes;
    }

    public static void setTables(List<Table> tables) {
        Restaurant.tables = tables;
    }

    public static void setUsers(List<User> users) {
        Restaurant.users = users;
    }

    public static List<Dish> getDishes() {
        return dishes;
    }

    public static List<Table> getTables() {
        return tables;
    }

    public Restaurant() {
    }

    public static List<Customer> getCustomerSaved() {
        return customerSaved;
    }

    public static List<Dish> getDishesServed() {
        return dishesServed;
    }

    public static void setCustomerSaved(List<Customer> customerSaved) {
        Restaurant.customerSaved = customerSaved;
    }

    public static void setDishesServed(List<Dish> dishesServed) {
        Restaurant.dishesServed = dishesServed;
    }
         
         
    
}

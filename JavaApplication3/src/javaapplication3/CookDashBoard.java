/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication3;

/**
 *
 * @author abdallah
 */
public class CookDashBoard {
    private int tableNumber;
    private String dishName;
    private int numberOfDishes;

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public int getNumberOfDishes() {
        return numberOfDishes;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setNumberOfDishes(int numberOfDishes) {
        this.numberOfDishes = numberOfDishes;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getDishName() {
        return dishName;
    }
    
    public CookDashBoard() {
    }

    public CookDashBoard(int tableNumber, String dishName, int numberOfDishes) {
        this.tableNumber = tableNumber;
        this.dishName = dishName;
        this.numberOfDishes = numberOfDishes;
    }

    
    
}

package javaapplication3;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.scene.image.ImageView;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.GridPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ComboBox;
import java.net.URL;
import javafx.application.Application;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class GUI extends Application {

    Stage window;
    Scene register, man1, emp1, dishes1, customertab1, menu, waiter1, customer1;
    TableView<CookDashBoard> table;
    TableView<User> table11;
    TableView<Table> table12;
    TableView<WaiterDashBoard> table15;
    private static Table tableL;
    private static Customer customer;
    private static Waiter waiter;
    private static Manager manager;
    private static Cook cook;
    private static final List<Dish> dishes = new ArrayList<>();
    private static TextArea textArea1 = new TextArea("Not reserved");
    private static TextArea textArea2 = new TextArea("Not reserved");
    private static TextArea textArea3 = new TextArea("Not reserved");
    private static TextArea textArea4 = new TextArea("Not reserved");
    private static TextArea textArea5 = new TextArea("Not reserved");
    private static TextArea textArea6 = new TextArea("Not reserved");
    private static TextArea textArea7 = new TextArea("Not reserved");
    private static final List<WaiterDashBoard> waiterBoard = new ArrayList<>();
    private static final List<CookDashBoard> cookBoard = new ArrayList<>();

    public ObservableList<CookDashBoard> getDish(CookDashBoard a) {
        ObservableList<CookDashBoard> dishes5 = table.getItems();
        dishes5.add(a);
        return dishes5;
    }

    public ObservableList<User> getUser() {
        ObservableList<User> users = FXCollections.observableArrayList();

        for (int i = 0; i < Restaurant.getUsers().size(); i++) {
            if (Restaurant.getUsers().get(i).getRole().equals("Client") || Restaurant.getUsers().get(i).getRole().equals("Manager")) {
                continue;
            }
            users.add(Restaurant.getUsers().get(i));
        }
        return users;
    }

    public ObservableList<Table> getTable() throws JDOMException, IOException {

        //Restaurant.loadTables();
        ObservableList<Table> tables = FXCollections.observableArrayList(Restaurant.getTables());
        return tables;
    }

    public ObservableList<WaiterDashBoard> getCustomer(WaiterDashBoard a) {
        ObservableList<WaiterDashBoard> customers = table15.getItems();
        customers.add(a);
        return customers;
    }

    @Override
    public void start(Stage primaryStage) throws JDOMException, IOException {
        window = primaryStage;
        lastSaved();
        final URL resource = getClass().getResource("music.mpeg");
        final Media media = new Media(resource.toString());
        final MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.play();
        mediaPlayer.setCycleCount(AudioClip.INDEFINITE);

        //  log scene
        GridPane grid1 = new GridPane();
        grid1.setVgap(10);
        grid1.setHgap(10);
        grid1.setPadding(new Insets(10, 10, 10, 10));
        Label userName1 = new Label("Enter user name:");
        Label password1 = new Label("Enter password:");
        Label no = new Label("If you do not have account");
        Label wel = new Label("Welcome to Java Restaurant");
        wel.setStyle("-fx-text-fill:Red; -fx-font-size:20px; -fx-font-weight:bold;");
        userName1.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        password1.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        no.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");

        TextField name = new TextField();
        PasswordField pass = new PasswordField();
        name.setMaxWidth(150);
        pass.setMaxWidth(150);
        Button login = new Button("Log In");
        login.setOnAction(e -> {
            try {
                String role = check(name, pass);
                String role1 = role.toLowerCase();
                switch (role1) {
                    case "client":
                        window.setScene(customer1);

                        name.clear();
                        pass.clear();
                        break;
                    case "waiter":
                        window.setScene(waiter1);
                        name.clear();
                        pass.clear();
                        break;
                    case "manager":
                        window.setScene(man1);
                        name.clear();
                        pass.clear();
                        break;
                    case "cooker":
                        window.setScene(dishes1);
                        name.clear();
                        pass.clear();
                        break;
                }
            } catch (JDOMException | IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
        Button reg = new Button("Register");
        Button exit = new Button("Exit");
        exit.setOnAction(e -> {
            window.close();
        });
        grid1.add(wel, 11, 2);
        grid1.add(userName1, 10, 17);
        grid1.add(password1, 10, 18);
        grid1.add(name, 11, 17);
        grid1.add(pass, 11, 18);
        grid1.add(login, 11, 19);
        grid1.add(no, 10, 20);
        grid1.add(reg, 11, 20);
        grid1.add(exit, 35, 35);
        grid1.setStyle("-fx-background-image: url('https://www.startlemusic.com/application/themes/rawnet/app/images/content/intro-banner.jpg')");
        Scene log1 = new Scene(grid1, 1000, 600);

        //register scene
        GridPane grid2 = new GridPane();
        grid2.setVgap(10);
        grid2.setHgap(5);
        Label userName2 = new Label("Enter your user name:");
        userName2.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        Label password2 = new Label("Enter your password:");
        password2.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        Label confirm = new Label("Re-Enter password:");
        confirm.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        Label name2 = new Label("Enter your name:");
        name2.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        Label role = new Label("Choose your role:");
        role.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        TextField name1 = new TextField();
        TextField usName = new TextField();
        usName.setMaxWidth(100);
        name1.setMaxWidth(100);
        PasswordField pass1 = new PasswordField();
        PasswordField pass2 = new PasswordField();
        pass1.setMaxWidth(100);
        pass2.setMaxWidth(100);
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().addAll("Manager", "Client", "Waiter", "Cooker");
        comboBox.setPromptText("Choose role");
        Button save = new Button("Sign up");

        save.setOnAction(e -> {
            String name12 = name1.getText();
            String role12 = comboBox.getValue();
            String userName12 = usName.getText();
            String pass12 = pass1.getText();
            String pass22 = pass2.getText();
            if (name1.getText().length() == 0 || comboBox.getValue() == null || usName.getText().length() == 0 || pass1.getText().length() == 0 || pass2.getText().length() == 0) {
                if (name12.length() == 0) {
                    AlertBox.display("Message", "Please enter name");
                } else if (userName12.length() == 0) {
                    AlertBox.display("Message", "Please enter username");
                } else if (pass12.length() == 0) {
                    AlertBox.display("Message", "Please enter password");
                } else if (pass22.length() == 0) {
                    AlertBox.display("Message", "Please re-enter your password");
                } else if (!pass12.equals(pass22)) {
                    AlertBox.display("Message", "Re-enter password is not same as password");
                } else if (role12 == null) {
                    AlertBox.display("Message", "Please select role");
                }
            } else {
                try {
                    if (checkusername(userName12, pass12) == 1) {

                    } else {
                        User a = new User(name12, role12, userName12, pass12);
                        try {
                            Restaurant.write(a);
                            AlertBox.display("Message", "Registered Successfully");
                            window.setScene(log1);
                            name1.clear();
                            usName.clear();
                            pass1.clear();
                            pass2.clear();
                        } catch (JDOMException | IOException ex) {
                            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } catch (JDOMException | IOException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        Button back = new Button("Back");
        back.setOnAction(e -> {
            window.setScene(register);
        });
        Label fin = new Label("Password must be at least 6 characters");
        fin.setStyle("-fx-text-fill:black; -fx-font-size:20px; -fx-font-weight:bold;");
        grid2.add(name2, 12, 15);
        grid2.add(name1, 13, 15);
        grid2.add(userName2, 12, 16);
        grid2.add(usName, 13, 16);
        grid2.add(fin,12,17);
        grid2.add(password2, 12, 18);
        grid2.add(pass1, 13, 18);
        grid2.add(confirm, 12, 19);
        grid2.add(pass2, 13, 19);
        grid2.add(role, 12, 20);
        grid2.add(comboBox, 13, 20);
        grid2.add(save, 30, 25);
        grid2.add(back, 31, 25);
        grid2.setStyle("-fx-background-image: url('https://www.startlemusic.com/application/themes/rawnet/app/images/content/intro-banner.jpg')");
        register = new Scene(grid2, 800, 550);

        reg.setOnAction(e -> {
            window.setScene(register);
        });

        // table manager scene 
        GridPane grid3 = new GridPane();
        grid3.setPadding(new Insets(30, 30, 30, 30));
        Image image = new Image(getClass().getResourceAsStream("index.jpg"));
        //labels icon
        Label t1 = new Label("Table 1", new ImageView(image));
        Label t2 = new Label("Table 2", new ImageView(image));
        Label t3 = new Label("Table 3", new ImageView(image));
        Label t4 = new Label("Table 4", new ImageView(image));
        Label t5 = new Label("Table 5", new ImageView(image));
        Label t6 = new Label("Table 6", new ImageView(image));
        Label t7 = new Label("Table 7", new ImageView(image));

        t1.setStyle("-fx-font-weight:bold; ");
        t2.setStyle("-fx-font-weight:bold; ");
        t3.setStyle("-fx-font-weight:bold; ");
        t4.setStyle("-fx-font-weight:bold; ");
        t5.setStyle("-fx-font-weight:bold; ");
        t6.setStyle("-fx-font-weight:bold; ");
        t7.setStyle("-fx-font-weight:bold; ");

        textArea1.setPrefHeight(200);
        textArea1.setPrefWidth(350);
        textArea2.setPrefHeight(200);
        textArea2.setPrefWidth(350);
        textArea3.setPrefHeight(200);
        textArea3.setPrefWidth(350);
        textArea4.setPrefHeight(200);
        textArea4.setPrefWidth(350);
        textArea5.setPrefHeight(200);
        textArea5.setPrefWidth(350);
        textArea6.setPrefHeight(200);
        textArea6.setPrefWidth(350);
        textArea7.setPrefHeight(200);
        textArea7.setPrefWidth(350);

        textArea1.setEditable(false);
        textArea2.setEditable(false);
        textArea3.setEditable(false);
        textArea4.setEditable(false);
        textArea5.setEditable(false);
        textArea6.setEditable(false);
        textArea7.setEditable(false);

        textArea1.setWrapText(true);
        textArea2.setWrapText(true);
        textArea3.setWrapText(true);
        textArea4.setWrapText(true);
        textArea5.setWrapText(true);
        textArea6.setWrapText(true);
        textArea7.setWrapText(true);

        Button logOut = new Button("Log out");
        Button back3 = new Button("Back");

        grid3.add(t1, 1, 1);
        grid3.add(textArea1, 20, 2);

        grid3.add(t2, 21, 1);
        grid3.add(textArea2, 41, 2);

        grid3.add(t3, 1, 6);
        grid3.add(textArea3, 20, 7);

        grid3.add(t4, 21, 6);
        grid3.add(textArea4, 41, 7);

        grid3.add(t5, 1, 11);
        grid3.add(textArea5, 20, 12);

        grid3.add(t6, 21, 11);
        grid3.add(textArea6, 41, 12);

        grid3.add(t7, 1, 16);
        grid3.add(textArea7, 20, 17);
        grid3.add(back3, 56, 26);
        grid3.add(logOut, 54, 26);
        grid3.setStyle("-fx-background-image: url('https://www.solidbackgrounds.com/images/2560x1440/2560x1440-yellow-orange-solid-color-background.jpg')");
        Scene tab1 = new Scene(grid3, 1200, 600);

        // manager scene        
        GridPane grid4 = new GridPane();
        grid4.setVgap(10);
        grid4.setHgap(10);
        Button tables1 = new Button("Tables");
        Button empList = new Button("Employee list");
        empList.setOnAction(e -> {
            table11.setItems(getUser());
            window.setScene(emp1);
        });
        Label totalMoney = new Label("Total money earned today :" + Manager.getEarnedMoney());
        totalMoney.setStyle("-fx-text-fill:black; -fx-font-weight:bold;");
        Label money = new Label("");
        money.setStyle("-fx-text-fill:black; -fx-font-weight:bold;");
        Button logOut1 = new Button("Log out");
        tables1.setOnAction(e -> {
            window.setScene(tab1);
            window.setTitle("Restaurant Tables");
            window.show();
        });
        grid4.add(tables1, 8, 15);
        grid4.add(empList, 16, 15);
        grid4.add(totalMoney, 8, 6);
        grid4.add(money, 9, 6);
        grid4.add(logOut1, 16, 40);
        grid4.setStyle("-fx-background-image: url('https://images.all-free-download.com/images/graphiclarge/red_shading_background_02_hd_pictures_169761.jpg')");

        man1 = new Scene(grid4, 600, 600);

        //user scene 
        GridPane grid6 = new GridPane();
        grid6.setPadding(new Insets(10, 10, 10, 10));
        TableColumn<User, String> nCol = new TableColumn<>("Name");
        nCol.setMinWidth(200);
        nCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<User, String> rCol = new TableColumn<>("Role");
        rCol.setMinWidth(200);
        rCol.setCellValueFactory(new PropertyValueFactory<>("role"));
        Button back2 = new Button("Back");
        back2.setOnAction(e -> {
            window.setScene(man1);
        });

        table11 = new TableView<>();
        table11.getColumns().addAll(nCol, rCol);

        grid6.setStyle("-fx-background-image: url('https://www.startlemusic.com/application/themes/rawnet/app/images/content/intro-banner.jpg')");
        grid6.add(table11, 1, 1);
        grid6.add(back2, 1, 3);
        emp1 = new Scene(grid6, 850, 537);

        //dishes scene 
        VBox vbox2 = new VBox();
        vbox2.setPadding(new Insets(10, 10, 10, 10));
        TableColumn<CookDashBoard, String> nameCol = new TableColumn<>("Dish Name");
        nameCol.setMinWidth(200);
        nameCol.setCellValueFactory(new PropertyValueFactory<>("dishName"));

        TableColumn<CookDashBoard, Integer> numberCol = new TableColumn<>("Number of dishes");
        numberCol.setMinWidth(200);
        numberCol.setCellValueFactory(new PropertyValueFactory<>("numberOfDishes"));

        TableColumn<CookDashBoard, Integer> timeCol = new TableColumn<>("Table number");
        timeCol.setMinWidth(200);
        timeCol.setCellValueFactory(new PropertyValueFactory<>("tableNumber"));
        
        Label instructions1 = new Label("Choose dish first then press Cooked and ready button");
        instructions1.setStyle("-fx-font-weight:bold ;");
        Button logOut2 = new Button("Log out");
        Button cooked = new Button("Cooked and ready");
        cooked.setOnAction(e -> {
            try {
                cookButtonClicked();
            } catch (JDOMException | IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        table = new TableView<>();
        table.getColumns().addAll(timeCol, nameCol, numberCol);
        HBox hbox2 = new HBox();
        hbox2.setSpacing(10);
        hbox2.setPadding(new Insets(10, 10, 10, 10));
        hbox2.setAlignment(Pos.BOTTOM_RIGHT);
        hbox2.getChildren().addAll(cooked, logOut2);
        vbox2.getChildren().addAll(instructions1,table, hbox2);
        dishes1 = new Scene(vbox2);

        // waiter dashboard scene
        VBox vbox1 = new VBox();
        vbox1.setPadding(new Insets(10, 10, 10, 10));
        TableColumn<WaiterDashBoard, String> name1Col = new TableColumn<>("Customer Name");
        name1Col.setMinWidth(200);
        name1Col.setCellValueFactory(new PropertyValueFactory<>("customerName"));

        TableColumn<WaiterDashBoard, Integer> number1Col = new TableColumn<>("Table number");
        number1Col.setMinWidth(200);
        number1Col.setCellValueFactory(new PropertyValueFactory<>("tableNumber"));
        Label instructions2 = new Label("Choose table first then press served button");
        instructions2.setStyle("-fx-font-weight:bold ;");
        Button logOut3 = new Button("Log out");
        logOut3.setOnAction(e -> {
            window.setScene(log1);
        });
        Button served = new Button("Served");
        served.setOnAction(e -> {
            try {
                servedButtonClicked();
            } catch (JDOMException | IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        table15 = new TableView<>();
        table15.getColumns().addAll(name1Col, number1Col);
        HBox hbox1 = new HBox();
        hbox1.setPadding(new Insets(10, 10, 10, 10));
        hbox1.setAlignment(Pos.BOTTOM_RIGHT);
        hbox1.getChildren().addAll(served, logOut3);
        vbox1.getChildren().addAll(instructions2,table15, hbox1);
        waiter1 = new Scene(vbox1);

        //customer tables scene
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10, 10, 10, 10));
        TableColumn<Table, Integer> number10 = new TableColumn<>("Table number");
        number10.setMinWidth(100);
        number10.setCellValueFactory(new PropertyValueFactory<>("tablenumber"));

        TableColumn<Table, Integer> seats1 = new TableColumn<>("seats");
        seats1.setMinWidth(100);
        seats1.setCellValueFactory(new PropertyValueFactory<>("tableseats"));

        TableColumn<Table, Boolean> smoke1 = new TableColumn<>("Smoking");
        smoke1.setMinWidth(200);
        smoke1.setCellValueFactory(new PropertyValueFactory<>("smokingornot"));

        TableColumn<Table, Boolean> reserved = new TableColumn<>("Reservation");
        reserved.setMinWidth(100);
        reserved.setCellValueFactory(new PropertyValueFactory<>("reservedornot"));
        
        Label instructions3 = new Label("Choose table first then press Reserve and order dishes");
       instructions3.setStyle("-fx-font-weight:bold ;");
        table12 = new TableView<>();
        table12.setItems(getTable());
        table12.getColumns().addAll(number10, seats1, smoke1, reserved);
        Button logOut4 = new Button("Log out");
        logOut4.setOnAction(e -> {
            window.setScene(log1);
        });
        Button reserve2 = new Button("Reserve and order dishes ");
        reserve2.setOnAction(e -> {
            table12.setItems(reserveButtonClicked());
        });
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(10, 10, 10, 10));
        hbox.setSpacing(10);
        hbox.setAlignment(Pos.BOTTOM_RIGHT);
        hbox.getChildren().addAll(reserve2, logOut4);
        vbox.getChildren().addAll(instructions3,table12, hbox);
        customertab1 = new Scene(vbox);

        // menu scene
        GridPane grid8 = new GridPane();
        grid8.setVgap(20);
        grid8.setHgap(10);
        grid8.setPadding(new Insets(10, 10, 10, 10));
        grid8.setStyle("-fx-background-image: url('https://shalimarindia.com/wp-content/uploads/2019/11/web_slider_background1.jpg')");
        menu = new Scene(grid8, 1900, 802);
        Label instructions4 = new Label("Enter number of dishes first then press order ");
       instructions4.setStyle("-fx-font-weight:bold ;");
        Button logOut5 = new Button("Log out");
        logOut5.setOnAction(e -> {
            table12.setItems(logOutButtonClicked());
            GUI.dishes.clear();
            window.setScene(log1);
        });
        Button exit1 = new Button("Exit");
        exit1.setOnAction(e -> {
            table12.setItems(logOutButtonClicked());
            window.close();
        });

        Label taxes = new Label("Appetizer (10% taxes), Main Course (15% taxes) and Desert (20% taxes)");
        taxes.setStyle("-fx-font-size: 20px; -fx-font-weight:bold;");
        Restaurant.load();
        Label dishName1 = new Label("Dish name : " + Restaurant.getDishes().get(0).getDishname());
        dishName1.setStyle("-fx-font-weight:bold; ");
        Label dishName2 = new Label("Dish name : " + Restaurant.getDishes().get(1).getDishname());
        dishName2.setStyle("-fx-font-weight:bold; ");
        Label dishName3 = new Label("Dish name : " + Restaurant.getDishes().get(2).getDishname());
        dishName3.setStyle("-fx-font-weight:bold; ");
        Label dishName4 = new Label("Dish name : " + Restaurant.getDishes().get(3).getDishname());
        dishName4.setStyle("-fx-font-weight:bold; ");
        Label dishName5 = new Label("Dish name : " + Restaurant.getDishes().get(4).getDishname());
        dishName5.setStyle("-fx-font-weight:bold; ");
        Label dishName6 = new Label("Dish name : " + Restaurant.getDishes().get(5).getDishname());
        dishName6.setStyle("-fx-font-weight:bold; ");
        Label dishName7 = new Label("Dish name : " + Restaurant.getDishes().get(6).getDishname());
        dishName7.setStyle("-fx-font-weight:bold; ");

        Label dishSort1 = new Label("Dish sort : " + Restaurant.getDishes().get(0).getDishsort());
        dishSort1.setStyle("-fx-font-weight:bold; ");
        Label dishSort2 = new Label("Dish sort : " + Restaurant.getDishes().get(1).getDishsort());
        dishSort2.setStyle("-fx-font-weight:bold; ");
        Label dishSort3 = new Label("Dish sort : " + Restaurant.getDishes().get(2).getDishsort());
        dishSort3.setStyle("-fx-font-weight:bold; ");
        Label dishSort4 = new Label("Dish sort : " + Restaurant.getDishes().get(3).getDishsort());
        dishSort4.setStyle("-fx-font-weight:bold; ");
        Label dishSort5 = new Label("Dish sort : " + Restaurant.getDishes().get(4).getDishsort());
        dishSort5.setStyle("-fx-font-weight:bold; ");
        Label dishSort6 = new Label("Dish sort : " + Restaurant.getDishes().get(5).getDishsort());
        dishSort6.setStyle("-fx-font-weight:bold; ");
        Label dishSort7 = new Label("Dish sort : " + Restaurant.getDishes().get(6).getDishsort());
        dishSort7.setStyle("-fx-font-weight:bold; ");

        Label dishPrice1 = new Label("Dish price :" + Restaurant.getDishes().get(0).getDishprice());
        dishPrice1.setStyle("-fx-font-weight:bold; ");
        Label dishPrice2 = new Label("Dish price :" + Restaurant.getDishes().get(1).getDishprice());
        dishPrice2.setStyle("-fx-font-weight:bold; ");
        Label dishPrice3 = new Label("Dish price :" + Restaurant.getDishes().get(2).getDishprice());
        dishPrice3.setStyle("-fx-font-weight:bold; ");
        Label dishPrice4 = new Label("Dish price :" + Restaurant.getDishes().get(3).getDishprice());
        dishPrice4.setStyle("-fx-font-weight:bold; ");
        Label dishPrice5 = new Label("Dish price :" + Restaurant.getDishes().get(4).getDishprice());
        dishPrice5.setStyle("-fx-font-weight:bold; ");
        Label dishPrice6 = new Label("Dish price :" + Restaurant.getDishes().get(5).getDishprice());
        dishPrice6.setStyle("-fx-font-weight:bold; ");
        Label dishPrice7 = new Label("Dish price :" + Restaurant.getDishes().get(6).getDishprice());
        dishPrice7.setStyle("-fx-font-weight:bold; ");
        TextField quantity = new TextField();

        Button order1 = new Button("Order");
        order1.setOnAction(e -> {
            boolean x = isInt(quantity);
            int quantity1 = initialiseQuantity(x, quantity);
            if (quantity1 == 0) {
                AlertBox.display("Message", "Please enter number of dishes");
            } else {
                Dish a = new MainCourse(Restaurant.getDishes().get(0).getDishname(), Restaurant.getDishes().get(0).getDishsort(), Restaurant.getDishes().get(0).getDishprice(), quantity1);
                double price = a.calcTotalPrice(a.getDishprice(), quantity1);
                GUI.dishes.add(a);
                AlertBox.display("Message", "Ordered Successfully");
                quantity.clear();
            }
        });
        Button order2 = new Button("Order");
        order2.setOnAction(e -> {
            boolean x = isInt(quantity);
            int quantity1 = initialiseQuantity(x, quantity);
            if (quantity1 == 0) {
                AlertBox.display("Message", "Please enter number of dishes");
            } else {
                Dish a = new Appetizer(Restaurant.getDishes().get(1).getDishname(), Restaurant.getDishes().get(1).getDishsort(), Restaurant.getDishes().get(1).getDishprice(), quantity1);
                double price = a.calcTotalPrice(a.getDishprice(), quantity1);
                GUI.dishes.add(a);
                AlertBox.display("Message", "Ordered Successfully");
                quantity.clear();

            }

        });
        Button order3 = new Button("Order");
        order3.setOnAction(e -> {
            boolean x = isInt(quantity);
            int quantity1 = initialiseQuantity(x, quantity);
            if (quantity1 == 0) {
                AlertBox.display("Message", "Please enter number of dishes");

            } else {
                Dish a = new Appetizer(Restaurant.getDishes().get(2).getDishname(), Restaurant.getDishes().get(2).getDishsort(), Restaurant.getDishes().get(2).getDishprice(), quantity1);
                double price = a.calcTotalPrice(a.getDishprice(), quantity1);
                GUI.dishes.add(a);
                AlertBox.display("Message", "Ordered Successfully");
                quantity.clear();

            }

        });
        Button order4 = new Button("Order");
        order4.setOnAction(e -> {
            boolean x = isInt(quantity);
            int quantity1 = initialiseQuantity(x, quantity);
            if (quantity1 == 0) {
                AlertBox.display("Message", "Please enter number of dishes");
            } else {
                Dish a = new Dessert(Restaurant.getDishes().get(3).getDishname(), Restaurant.getDishes().get(3).getDishsort(), Restaurant.getDishes().get(3).getDishprice(), quantity1);
                double price = a.calcTotalPrice(a.getDishprice(), quantity1);
                GUI.dishes.add(a);
                AlertBox.display("Message", "Ordered Successfully");
                quantity.clear();

            }
        });
        Button order5 = new Button("Order");
        order5.setOnAction(e -> {
            boolean x = isInt(quantity);
            int quantity1 = initialiseQuantity(x, quantity);
            if (quantity1 == 0) {
                AlertBox.display("Message", "Please enter number of dishes");
            } else {
                Dish a = new Dessert(Restaurant.getDishes().get(4).getDishname(), Restaurant.getDishes().get(4).getDishsort(), Restaurant.getDishes().get(4).getDishprice(), quantity1);
                double price = a.calcTotalPrice(a.getDishprice(), quantity1);
                GUI.dishes.add(a);
                AlertBox.display("Message", "Ordered Successfully");
                quantity.clear();

            }
        });
        Button order6 = new Button("Order");
        order6.setOnAction(e -> {
            boolean x = isInt(quantity);
            int quantity1 = initialiseQuantity(x, quantity);
            if (quantity1 == 0) {
                AlertBox.display("Message", "Please enter number of dishes");
            } else {
                Dish a = new MainCourse(Restaurant.getDishes().get(5).getDishname(), Restaurant.getDishes().get(5).getDishsort(), Restaurant.getDishes().get(5).getDishprice(), quantity1);
                double price = a.calcTotalPrice(a.getDishprice(), quantity1);
                GUI.dishes.add(a);
                AlertBox.display("Message", "Ordered Successfully");
                quantity.clear();

            }
        });
        Button order7 = new Button("Order");
        order7.setOnAction(e -> {
            boolean x = isInt(quantity);
            int quantity1 = initialiseQuantity(x, quantity);
            if (quantity1 == 0) {
                AlertBox.display("Message", "Please enter number of dishes");
            } else {
                Dish a = new MainCourse(Restaurant.getDishes().get(6).getDishname(), Restaurant.getDishes().get(6).getDishsort(), Restaurant.getDishes().get(6).getDishprice(), quantity1);
                double price = a.calcTotalPrice(a.getDishprice(), quantity1);
                GUI.dishes.add(a);
                AlertBox.display("Message", "Ordered Successfully");
                quantity.clear();
            }
        });

        grid8.add(taxes, 12, 1);
        grid8.add(instructions4,12,2);
        
        grid8.add(dishName1, 1, 6);
        grid8.add(dishSort1, 12, 6);
        grid8.add(dishPrice1, 1, 7);
        grid8.add(order1, 12, 7);

        grid8.add(dishName2, 15, 6);
        grid8.add(dishSort2, 19, 6);
        grid8.add(dishPrice2, 15, 7);
        grid8.add(order2, 19, 7);

        grid8.add(dishName3, 1, 8);
        grid8.add(dishSort3, 12, 8);
        grid8.add(dishPrice3, 1, 9);
        grid8.add(order3, 12, 9);

        grid8.add(dishName4, 15, 8);
        grid8.add(dishSort4, 19, 8);
        grid8.add(dishPrice4, 15, 9);
        grid8.add(order4, 19, 9);

        grid8.add(dishName5, 1, 10);
        grid8.add(dishSort5, 12, 10);
        grid8.add(dishPrice5, 1, 11);
        grid8.add(order5, 12, 11);

        grid8.add(dishName6, 15, 10);
        grid8.add(dishSort6, 19, 10);
        grid8.add(dishPrice6, 15, 11);
        grid8.add(order6, 19, 11);

        grid8.add(dishName7, 1, 12);
        grid8.add(dishSort7, 12, 12);
        grid8.add(dishPrice7, 1, 13);
        grid8.add(order7, 12, 13);

        quantity.setMaxWidth(50);

        Label tMoney = new Label("0.0");
        Button calc = new Button("Total money");
        calc.setOnAction(e -> {
            tMoney.setText(String.valueOf(Dish.totalPrice));
        });
        Button done = new Button("Done");
        done.setOnAction(e -> {
            if (tableL.getTablenumber() == 1) {
                textArea1.setText("Reserved by " + GUI.customer.getName() + "\n" + "Dishes are: " + "\n");
                for (int i = 0; i < GUI.dishes.size(); i++) {
                    int temp = i + 1;
                    textArea1.appendText(temp + "." + GUI.dishes.get(i).getDishname() + " " + "Quantity: " + GUI.dishes.get(i).getNumberofdishes() + "\n");
                }
            } else if (tableL.getTablenumber() == 2) {
                textArea2.setText("Reserved by " + GUI.customer.getName() + "\n" + "Dishes are: " + "\n");
                for (int i = 0; i < GUI.dishes.size(); i++) {
                    int temp = i + 1;
                    textArea2.appendText(temp + "." + GUI.dishes.get(i).getDishname() + " " + "Quantity: " + GUI.dishes.get(i).getNumberofdishes() + "\n");
                }
            } else if (tableL.getTablenumber() == 3) {
                textArea3.setText("Reserved by " + GUI.customer.getName() + "\n" + "Dishes are: " + "\n");
                for (int i = 0; i < GUI.dishes.size(); i++) {
                    int temp = i + 1;
                    textArea3.appendText(temp + "." + GUI.dishes.get(i).getDishname() + " " + "Quantity: " + GUI.dishes.get(i).getNumberofdishes() + "\n");
                }
            } else if (tableL.getTablenumber() == 4) {
                textArea4.setText("Reserved by " + GUI.customer.getName() + "\n" + "Dishes are: " + "\n");
                for (int i = 0; i < GUI.dishes.size(); i++) {
                    int temp = i + 1;
                    textArea4.appendText(temp + "." + GUI.dishes.get(i).getDishname() + " " + "Quantity: " + GUI.dishes.get(i).getNumberofdishes() + "\n");
                }
            } else if (tableL.getTablenumber() == 5) {
                textArea5.setText("Reserved by " + GUI.customer.getName() + "\n" + "Dishes are: " + "\n");
                for (int i = 0; i < GUI.dishes.size(); i++) {
                    int temp = i + 1;
                    textArea5.appendText(temp + "." + GUI.dishes.get(i).getDishname() + " " + "Quantity: " + GUI.dishes.get(i).getNumberofdishes() + "\n");
                }
            } else if (tableL.getTablenumber() == 6) {
                textArea6.setText("Reserved by " + GUI.customer.getName() + "\n" + "Dishes are: " + "\n");
                for (int i = 0; i < GUI.dishes.size(); i++) {
                    int temp = i + 1;
                    textArea6.appendText(temp + "." + GUI.dishes.get(i).getDishname() + " " + "Quantity: " + GUI.dishes.get(i).getNumberofdishes() + "\n");
                }
            } else if (tableL.getTablenumber() == 7) {
                textArea7.setText("Reserved by " + GUI.customer.getName() + "\n" + "Dishes are: " + "\n");
                for (int i = 0; i < GUI.dishes.size(); i++) {
                    int temp = i + 1;
                    textArea7.appendText(temp + "." + GUI.dishes.get(i).getDishname() + " " + "Quantity: " + GUI.dishes.get(i).getNumberofdishes() + "\n");
                }
            }
            WaiterDashBoard waiterDashBoard = new WaiterDashBoard(customer.getName(), tableL.getTablenumber());
            table15.setItems(getCustomer(waiterDashBoard));
            for (int j = 0; j < GUI.dishes.size(); j++) {
                CookDashBoard cookDashBoard = new CookDashBoard(tableL.getTablenumber(), GUI.dishes.get(j).getDishname(), GUI.dishes.get(j).getNumberofdishes());
                table.setItems(getDish(cookDashBoard));
            }

            if (GUI.dishes.isEmpty()) {
                AlertBox.display("Message", "You didnot make orders");
            } else {
                AlertBox.display("Message", "Dish orders reserved successfully");
                Manager.setEarnedMoney(Manager.getEarnedMoney() + Dish.totalPrice);
                totalMoney.setText("Total money earned today :" + Manager.getEarnedMoney());
                try {
                    Restaurant.writeRecent(customer.getName(), tableL.getTablenumber(),Dish.totalPrice, dishes);
                } catch (JDOMException | IOException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                window.setScene(customer1);
                tMoney.setText("0.0");
                GUI.dishes.clear();
                Dish.totalPrice = 0;
            }
        });
        Label quantity1 = new Label("Enter number of dishes");
        quantity1.setStyle("-fx-font-weight:bold; ");
        grid8.add(quantity1, 15, 12);
        grid8.add(quantity, 19, 12);
        grid8.add(calc, 15, 15);
        grid8.add(tMoney, 19, 15);
        grid8.add(done, 24, 20);
        grid8.add(logOut5, 25, 20);
        grid8.add(exit1, 26, 20);

        // customer scene
        GridPane grid9 = new GridPane();
        Button checkOut = new Button("Check out");
        Button log = new Button("Logout");
        log.setOnAction(e -> {
            window.setScene(log1);
            window.setTitle("Restaurant");
            window.show();
        });
        Button exit2 = new Button("Exit");
        exit2.setOnAction(e -> {
            window.close();
        });
        Button reserve = new Button("Reserve a table");
        reserve.setOnAction(e -> {

            window.setScene(customertab1);
            window.setTitle("Restaurant");
            window.show();
        });

        grid9.add(reserve,8, 25);
        grid9.setVgap(10);
        grid9.setHgap(10);
        grid9.add(checkOut, 16, 25);
        grid9.add(exit2, 20, 45);
        grid9.add(log, 19, 45);
        grid9.setStyle("-fx-background-image: url('https://i.pinimg.com/736x/e3/a5/d0/e3a5d05f404c56afa57c023f4ba44612.jpg')");

        customer1 = new Scene(grid9, 600, 600);

        //checkout scene
        GridPane grid10 = new GridPane();
        grid10.setVgap(10);
        grid10.setHgap(5);
        Label enter = new Label("Choose your table number ");
        enter.setStyle("-fx-font-weight:bold; ");
        ComboBox<Integer> combo = new ComboBox();
        combo.getItems().addAll(1, 2, 3, 4, 5, 6, 7);
        combo.setPromptText("table number");
        Button ok = new Button("Okay");
        Button back20 = new Button("Back");
        back20.setOnAction(e -> {
            window.setScene(customer1);
        });
        ok.setOnAction(e -> {

            int tableNumber = combo.getValue();
            int index = tableNumber - 1;
            if (Restaurant.getTables().get(index).getReservedornot() == true) {
                Restaurant.getTables().get(index).setReservedornot(false);
                if (tableNumber == 1) {
                    textArea1.setText("Not reserved");
                } else if (tableNumber == 2) {
                    textArea2.setText("Not reserved");
                } else if (tableNumber == 3) {
                    textArea3.setText("Not reserved");
                } else if (tableNumber == 4) {
                    textArea4.setText("Not reserved");
                } else if (tableNumber == 5) {
                    textArea5.setText("Not reserved");
                } else if (tableNumber == 6) {
                    textArea6.setText("Not reserved");
                } else if (tableNumber == 7) {
                    textArea7.setText("Not reserved");
                }
                table15.setItems(removeWaiterDashBoard(tableNumber));
                table.setItems(removeCookDashBoard(tableNumber));
                table12.refresh();
                AlertBox.display("Message", "Thank You");
                try {
                    Restaurant.modifyCheckOut(customer.getName(),tableNumber);
                } catch (JDOMException | IOException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                window.setScene(customer1);
            } else {
                AlertBox.display("Message", "Table wasnot reserved");
            }

        });
        grid10.add(enter, 10, 35);
        grid10.add(combo, 12, 35);
        grid10.add(back20, 11, 38);
        grid10.add(ok, 12, 38);
        grid10.setStyle("-fx-background-image: url('https://i.pinimg.com/736x/e3/a5/d0/e3a5d05f404c56afa57c023f4ba44612.jpg')");
        Scene end = new Scene(grid10, 600, 600);
        checkOut.setOnAction(e -> {
            window.setScene(end);
            window.setResizable(false);
            window.setTitle("Check Out");
            window.show();
        });

        window.setScene(log1);  
        window.setTitle("Restaurant");
        window.show();
        window.setResizable(false);
        reg.setOnAction(e -> {
            window.setScene(register);
            window.setTitle("Restaurant Register");
            window.show();
        });
        back.setOnAction(e -> {
            window.setScene(log1);
            window.setTitle("Restaurant");
            window.show();
        });
        back3.setOnAction(e -> {
            window.setScene(man1);
            window.setTitle("Restaurant");
            window.show();
        });

        logOut.setOnAction(e -> {
            window.setScene(log1);
            window.setTitle("Restaurant");
            window.show();
        });
        logOut1.setOnAction(e -> {
            window.setScene(log1);
            window.setTitle("Restaurant");
            window.show();
        });
        logOut2.setOnAction(e -> {
            window.setScene(log1);
            window.setTitle("Restaurant");
            window.show();
        });
        back2.setOnAction(e -> {
            window.setScene(man1);
            window.setTitle("Restaurant");
            window.show();
        });
        table15.setItems(lastCustomer());
        table.setItems(lastCook());
        GUI.waiterBoard.clear();
        GUI.cookBoard.clear();
        
    }

    public static void main(String[] args) {
        launch(args);
    }

    public String check(TextField input, PasswordField input1) throws JDOMException, IOException {
        Restaurant.load();
        for (int temp = 0; temp < Restaurant.getUsers().size(); temp++) {

            if (Restaurant.getUsers().get(temp).getUsername().equals(input.getText()) && Restaurant.getUsers().get(temp).getPassword().equals(input1.getText())) {
                switch (Restaurant.getUsers().get(temp).getRole()) {
                    case "Client":
                        GUI.customer = new Customer();
                        GUI.customer.setName(Restaurant.getUsers().get(temp).getName());
                        GUI.customer.setRole("Client");
                        break;
                    case "Manager":
                        GUI.manager = new Manager();
                        GUI.manager.setName(Restaurant.getUsers().get(temp).getName());
                        GUI.manager.setRole("Manager");
                        break;
                    case "Waiter":
                        GUI.waiter = new Waiter();
                        GUI.waiter.setName(Restaurant.getUsers().get(temp).getName());
                        GUI.waiter.setRole("Waiter");
                        break;
                    case "Cooker":
                        GUI.cook = new Cook();
                        GUI.cook.setName(Restaurant.getUsers().get(temp).getName());
                        GUI.cook.setRole("Cook");
                        break;
                }
                return Restaurant.getUsers().get(temp).getRole();
            }
        }
        AlertBox.display("Message", "Invalid username or password");
        return "0";

    }
    public ObservableList<Table> reserveButtonClicked(){
     ObservableList<Table> tableSelected,allTables;
     allTables=table12.getItems();
     allTables.sort(Comparator.comparing(Table::getTablenumber));
     tableSelected = table12.getSelectionModel().getSelectedItems();
     int index = table12.getSelectionModel().getSelectedIndex();
     if(Restaurant.getTables().get(index).getReservedornot()==true){
     AlertBox.display("Message", "Table is reserved");
     return allTables;
     }else{
     Restaurant.getTables().get(index).setReservedornot(true);
     allTables.add(Restaurant.getTables().get(index));
     GUI.tableL= Restaurant.getTables().get(index);
     tableSelected.forEach(allTables::remove);
     allTables.sort(Comparator.comparing(Table::getTablenumber));
     window.setScene(menu);
     window.show();
     table12.refresh();
     return allTables;
     }
     }

    

    public boolean isInt(TextField input) {
        try {
            int time = Integer.parseInt(input.getText());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public int initialiseQuantity(boolean x, TextField input) {
        if (x == true) {
            int time = Integer.parseInt(input.getText());
            return time;
        }
        return 0;
    }

    public ObservableList<WaiterDashBoard> servedButtonClicked() throws JDOMException, IOException {
        ObservableList<WaiterDashBoard> waiterDashBoardSelected, allWaiterDashBoard;
        allWaiterDashBoard = table15.getItems();
        waiterDashBoardSelected = table15.getSelectionModel().getSelectedItems();
        int index = table15.getSelectionModel().getSelectedIndex();
        int tableNumber= allWaiterDashBoard.get(index).getTableNumber();
        Restaurant.modifyServiced(tableNumber);
        waiterDashBoardSelected.forEach(allWaiterDashBoard::remove);
        return allWaiterDashBoard;
    }

    public ObservableList<CookDashBoard> cookButtonClicked() throws JDOMException, IOException {
        ObservableList<CookDashBoard> cookDashBoardSelected, allCookDashBoard;
        allCookDashBoard = table.getItems();
        cookDashBoardSelected = table.getSelectionModel().getSelectedItems();
        int index= table.getSelectionModel().getSelectedIndex();
        int tableNumber = allCookDashBoard.get(index).getTableNumber();
        Restaurant.modifyServed(tableNumber);
        cookDashBoardSelected.forEach(allCookDashBoard::remove);
        return allCookDashBoard;
    }

    public int checkusername(String input2, String input3) throws JDOMException, IOException {
        Restaurant.load();
        for (int temp = 0; temp < Restaurant.getUsers().size(); temp++) {

            if (input2.equals(Restaurant.getUsers().get(temp).getUsername())) {
                AlertBox.display("Message", "This user is already exist");
                return 1;

            }
        }
        if (input3.length() < 6) {
            AlertBox.display("Message", "Make sure password is at least 6 characters");
            return 1;
        }
        return 0;

    }

    public ObservableList<Table> logOutButtonClicked() {
        ObservableList<Table> allTables;
        allTables = table12.getItems();
        for (int i = 0; i < allTables.size(); i++) {
            if (GUI.tableL.getTablenumber() == allTables.get(i).getTablenumber() && GUI.tableL.getReservedornot() == true) {
                allTables.get(i).setReservedornot(false);
                Restaurant.getTables().get(i).setReservedornot(false);
            }
        }
        allTables.sort(Comparator.comparing(Table::getTablenumber));
        return allTables;
    }

    public ObservableList<WaiterDashBoard> removeWaiterDashBoard(int tableNumber) {
        ObservableList<WaiterDashBoard> allWaiterDashBoard;
        allWaiterDashBoard = table15.getItems();

        for (int i = 0; i < allWaiterDashBoard.size(); i++) {
            if (allWaiterDashBoard.get(i).getTableNumber() == tableNumber) {
                WaiterDashBoard a = table15.getItems().get(i);
                allWaiterDashBoard.remove(a);
                i = -1;
            }
        }

        return allWaiterDashBoard;
    }

    public ObservableList<CookDashBoard> removeCookDashBoard(int tableNumber) {
        ObservableList<CookDashBoard> allCookDashBoard;
        allCookDashBoard = table.getItems();
        int i;
        for (i = 0; i < allCookDashBoard.size(); i++) {
            if (allCookDashBoard.get(i).getTableNumber() == tableNumber) {
                CookDashBoard a = table.getItems().get(i);
                allCookDashBoard.remove(a);
                i = -1;
            }
        }
        return allCookDashBoard;
    }
    public void lastSaved() throws JDOMException, IOException {

        Restaurant.load();
        Restaurant.loadTables();
        File inputFile = new File("customer.xml");
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(inputFile);

        Element rootNode = document.getRootElement();

        List<Element> customersList = rootNode.getChildren("customer");
        for (int i = 0; i < customersList.size(); i++) {
            Element customerX = customersList.get(i);
            double totalPrice = Double.parseDouble(customerX.getChild("totalprice").getText());
            Manager.setEarnedMoney(Manager.getEarnedMoney() + totalPrice);
            String name;
            name = customerX.getChild("name").getText();
            Customer c = new Customer(name,"Client");
            int tableNumber = Integer.parseInt(customerX.getChild("tableNumber").getText());
            boolean checkOut1 = Boolean.parseBoolean(customerX.getChild("checkout").getText());
            for (int j = 0; j < Restaurant.getTables().size(); j++) {
                if (tableNumber == Restaurant.getTables().get(j).getTablenumber() && checkOut1 == false) {
                    Restaurant.getTables().get(j).setReservedornot(true);
                    break;
                }
            }
            Element dish1 = customerX.getChild("dishes");
            List<Element> dishesList = dish1.getChildren("dish");
            for (int k = 0; k < dishesList.size(); k++) {
                Element dish = dishesList.get(k);
                String dishName = dish.getChild("dishName").getText();
                float dishPrice = Float.parseFloat(dish.getChild("dishPrice").getText());
                String dishType = dish.getChild("dishType").getText();
                int quantity = Integer.parseInt(dish.getChild("quantity").getText());
                if (dishName.equals("Appetizer")) {
                    Dish a = new Appetizer(dishName, dishType, dishPrice, quantity);
                    GUI.dishes.add(a);
                } else if (dishName.equals("Main Course")) {
                    Dish a = new MainCourse(dishName, dishType, dishPrice, quantity);
                    GUI.dishes.add(a);
                } else {
                    Dish a = new Dessert(dishName, dishType, dishPrice, quantity);
                    GUI.dishes.add(a);
                }
            }
            if (tableNumber == 1) {
                textArea1.setText("Reserved by "+c.getName()+"\n"+"Dishes are: "+"\n");
                for (int m = 0; m < GUI.dishes.size(); m++) {
                    int temp = m + 1;
                    textArea1.appendText(temp+"."+GUI.dishes.get(m).getDishname()+" "+"Quantity: "+GUI.dishes.get(m).getNumberofdishes()+"\n");
                }
            } else if (tableNumber == 2) {
                textArea2.setText("Reserved by "+c.getName()+"\n"+"Dishes are: "+"\n");
                for (int m = 0; m < GUI.dishes.size(); m++) {
                    int temp = m + 1;
                    textArea2.appendText(temp+"."+GUI.dishes.get(m).getDishname()+" "+"Quantity: "+GUI.dishes.get(m).getNumberofdishes()+"\n");
                    //System.out.println("a");
                }
            } else if (tableNumber == 3) {
                textArea3.setText("Reserved by "+c.getName()+"\n"+"Dishes are: "+"\n");
                for (int m = 0; m < GUI.dishes.size(); m++) {
                    int temp = m + 1;
                    textArea3.appendText(temp+"."+GUI.dishes.get(m).getDishname()+" "+"Quantity: "+GUI.dishes.get(m).getNumberofdishes()+"\n");
                }
            } else if (tableNumber == 4) {
                textArea4.setText("Reserved by "+c.getName()+"\n"+"Dishes are: "+"\n");
                for (int m = 0; m < GUI.dishes.size(); m++) {
                    int temp = m + 1;
                    textArea4.appendText(temp+"."+GUI.dishes.get(m).getDishname()+" "+"Quantity: "+GUI.dishes.get(m).getNumberofdishes()+"\n");
                }
            } else if (tableNumber == 5) {
                textArea5.setText("Reserved by "+c.getName()+"\n"+"Dishes are: "+"\n");
                for (int m = 0; m < GUI.dishes.size(); m++) {
                    int temp = m + 1;
                    textArea5.appendText(temp+"."+GUI.dishes.get(m).getDishname()+" "+"Quantity: "+GUI.dishes.get(m).getNumberofdishes()+"\n");
                }
            } else if (tableNumber == 6) {
                textArea6.setText("Reserved by "+c.getName()+"\n"+"Dishes are: "+"\n");
                for (int m = 0; m < GUI.dishes.size(); m++) {
                    int temp = m + 1;
                    textArea6.appendText(temp+"."+GUI.dishes.get(m).getDishname()+" "+"Quantity: "+GUI.dishes.get(m).getNumberofdishes()+"\n");
                }
            } else   {
                textArea7.setText("Reserved by "+c.getName()+"\n"+"Dishes are: "+"\n");
                for (int m = 0; m < GUI.dishes.size(); m++) {
                    int temp = m + 1;
                    textArea7.appendText(temp+"."+GUI.dishes.get(m).getDishname()+" "+"Quantity: "+GUI.dishes.get(m).getNumberofdishes()+"\n");
                }
            }
            boolean serve = Boolean.parseBoolean(customerX.getChild("served").getText());
            boolean serviced = Boolean.parseBoolean(customerX.getChild("serviced").getText());
            if(serviced == false){
                WaiterDashBoard a = new WaiterDashBoard(c.getName(),tableNumber);
                GUI.waiterBoard.add(a);
            }
            if(serve == false){
                for(int n=0;n<GUI.dishes.size();n++){
                    CookDashBoard cookDashBoard = new CookDashBoard(tableNumber,GUI.dishes.get(n).getDishname(),GUI.dishes.get(n).getNumberofdishes());
                    GUI.cookBoard.add(cookDashBoard);
                }  
            }
            GUI.dishes.clear();
        }
   }
   public ObservableList<WaiterDashBoard> lastCustomer(){
       ObservableList<WaiterDashBoard> waiter20;
       waiter20=table15.getItems();
       for(int i=0;i<GUI.waiterBoard.size();i++){
           waiter20.add(GUI.waiterBoard.get(i));
       }
       
       return waiter20;
   }
   public ObservableList<CookDashBoard> lastCook(){
       ObservableList<CookDashBoard> cooker;
       cooker=table.getItems();
       for(int i=0;i<GUI.cookBoard.size();i++){
           cooker.add(GUI.cookBoard.get(i));
       }
       return cooker;
   }
}

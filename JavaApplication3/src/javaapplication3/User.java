package javaapplication3;
public class User {
         private String name;
         private String role;
         private String username;
         private String password;

    public User() {
    }

    public User(String name, String role, String username, String password) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.name = name;
    }
    public User(String name,String role)
    {
        this.name = name;
        this.role = role;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
         
}

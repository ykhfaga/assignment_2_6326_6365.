package javaapplication3;

public class Manager extends User  {
    private static double earnedMoney=0;

    public Manager(String name, String role) {
        super(name, role);
    }

    public Manager() {
    }

    public static void setEarnedMoney(double earnedMoney) {
        Manager.earnedMoney = earnedMoney;
    }

    public static double getEarnedMoney() {
        return earnedMoney;
    }
    
    
}
